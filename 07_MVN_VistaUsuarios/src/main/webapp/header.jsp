<%-- 
    Document   : header
    Created on : 21-feb-2019, 20:43:31
    Author     : IEUser
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="com.vn.appusuarios.modelo.Usuario"%>

<!-- Usuario usuario = (Usuario) session.getAttribute("usuario"); -->

<%-- Usar el bean "usuario" en la forma de JSTL --%>

<c:catch var="excepcionUsuario">

	<jsp:useBean id="usuario" type="com.vn.appusuarios.modelo.Usuario"
		scope="session">
		<jsp:getProperty property="*" name="usuario"></jsp:getProperty>
	</jsp:useBean>
</c:catch>

<header>
	<h2>Aplicaci�n�Gesti�n�MVC JSP</h2>
	<nav>
		<a href="index.jsp">Inicio</a>
		<c:set var="nombre" scope="session" value="${session.nombre}" />
		<c:set var="nombre2" scope="session" value="${sessionScope.nombre}" />
		<c:choose>
			<c:when test='<%=session.getAttribute("name") != null%>'>
				<a href="listar.jsp">Listar</a>
				<a href="logout.jsp">Logout</a>
				<h2 style="color:red">Est� iniciada la sesion de <%=session.getAttribute("name")%></h2>
			</c:when>
			<c:otherwise>
				<a href="login.jsp">Login</a>
				<a href="registrarse.jsp">Registrarse</a>
			</c:otherwise>
		</c:choose>
		
		
	</nav>
	<h3 style="color: red"></h3>
</header>