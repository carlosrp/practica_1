<%-- 
    Document   : index
    Created on : 21-feb-2019, 20:42:40
    Author     : IEUser
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="head.jsp" %>
<html>
    
    
    <body>
        <%@include file="header.jsp" %>
        <h1>Logout</h1> 
        <form name="form2" action="usuarios.do" method="post">

            <h5>¿Quieres cerrar sesion?</h5>
            
            <input type="submit" value="Si"/>
        </form>
        <br>
        <a href="index.jsp">No, llevame al index</a>
    </body>
</html>